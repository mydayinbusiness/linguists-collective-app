import React from 'react';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'perfect-scrollbar';

import { Mailbox } from 'components';

var mailps;

class DashboardMessagesPanel extends React.Component {
    componentDidMount() {
        if (navigator.platform.indexOf('Win') > -1) {
            mailps = new PerfectScrollbar(this.refs.mailboxlist, { suppressScrollX: true, suppressScrollY: false });
        }
    }
    componentWillUnmount() {
        if (navigator.platform.indexOf('Win') > -1) {
            mailps.destroy();
        }
    }

    render(){
        return (
            <div className="col-xl-8 col-lg-7 col-md-7 col-12">
                <section className="box">
                    <header className="panel_header">
                        <h2 className="title float-left">Messages</h2>
                    </header>
                    <div className="content-body">
                        <div className="row">
                        <div className="col-12">
                            <div className="wid-notification">
                                <ul className="list-unstyled notification-widget" ref="mailboxlist">
                                    <Mailbox mailbox={this.props.messages} />
                                </ul>
                            </div>
                        </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

DashboardMessagesPanel.propTypes = {
    messages: PropTypes.array
}

export default DashboardMessagesPanel;
