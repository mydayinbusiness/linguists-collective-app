import React from 'react';
import PropTypes from 'prop-types';

import { Polar } from 'react-chartjs-2';

class DashboardReferralsPanel extends React.Component {
    render() {
        return (
            <div className="col-12 col-lg-6 col-md-6 col-xl-4">
                <section className="box ">
                    <header className="panel_header">
                        <h2 className="title float-left">Referrals</h2>
                    </header>
                    <div className="content-body">
                        <div className="row">
                            <div className="col-12">
                                <div className="chart-container">
                                    <Polar
                                        data={this.props.data}
                                        options={this.props.options}
                                        height={this.props.height} />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

DashboardReferralsPanel.propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
    height: PropTypes.number
}

export default DashboardReferralsPanel;
