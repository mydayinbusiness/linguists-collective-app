import React from 'react';
import PropTypes from 'prop-types';

import { Line } from 'react-chartjs-2';

class DashboardSubscriptionsPanel extends React.Component {
    render() {
        return (
            <div className="col-12 col-lg-6 col-md-6 col-xl-8">
                <section className="box ">
                    <header className="panel_header">
                        <h2 className="title float-left">Subscriptions</h2>
                    </header>
                    <div className="content-body">
                        <div className="row">
                            <div className="col-12">
                                <div className="chart-container">
                                    <Line
                                        data={this.props.data}
                                        options={this.props.options}
                                        height={this.props.height} />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

DashboardSubscriptionsPanel.propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
    height: PropTypes.number
}

export default DashboardSubscriptionsPanel;
