import React from 'react';
import PropTypes from 'prop-types';

class DashboardHeading extends React.Component {
    render(){
        return (
            <div className="page-title">
                <div className="float-left">
                    <h1 className="title">{this.props.title}</h1>
                </div>
            </div>
        );
    }
}

DashboardHeading.propTypes = {
    title: PropTypes.string,
}

export default DashboardHeading;
