import React from 'react';
import PropTypes from 'prop-types';

import { HorizontalBar } from 'react-chartjs-2';

class DashboardProjectsPanel extends React.Component {
    render() {
        return (
            <div className="col-12 col-lg-5 col-md-5 col-xl-4">
                <section className="box ">
                    <header className="panel_header">
                        <h2 className="title float-left">Projects</h2>
                    </header>
                    <div className="content-body">
                        <div className="row">
                            <div className="col-12">
                                <div className="chart-container">
                                    <div className="chart-area">
                                        <HorizontalBar
                                            data={this.props.data}
                                            options={this.props.options}
                                            height={this.props.height} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

DashboardProjectsPanel.propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
    height: PropTypes.number
}

export default DashboardProjectsPanel;
