import React from 'react';
import PropTypes from 'prop-types';

class DashboardBox extends React.Component {
    render(){
        return (
            <div className="db_box icon_stats">
                <i className={` widicon float-left i-${this.props.icon} icon-pink`}></i>
                <div className="stats float-left">
                    <h2 className="widtitle">{this.props.value}</h2>
                    <span className="widtag">{this.props.title}</span>
                </div>
            </div>
        );
    }
}

DashboardBox.propTypes = {
    title: PropTypes.string,
    value: PropTypes.string,
    icon: PropTypes.string,
}

export default DashboardBox;
