import Admin from 'views/admin/Dashboard/Admin.jsx';

import Buyer from 'views/linguist/Buyer/Buyer.jsx';
import AddBuyer from 'views/linguist/Buyer/AddBuyer.jsx';
import EditBuyer from 'views/linguist/Buyer/EditBuyer.jsx';

import Seller from 'views/linguist/Seller/Seller.jsx';
import AddSeller from 'views/linguist/Seller/AddSeller.jsx';
import EditSeller from 'views/linguist/Seller/EditSeller.jsx';

import User from 'views/linguist/User/User.jsx';
import AddUser from 'views/linguist/User/AddUser.jsx';
import EditUser from 'views/linguist/User/EditUser.jsx';

import Invoice from 'views/linguist/Invoice/Invoice.jsx';
import AddInvoice from 'views/linguist/Invoice/AddInvoice.jsx';
import EditInvoice from 'views/linguist/Invoice/EditInvoice.jsx';


import Ticket from 'views/linguist/Ticket/Ticket.jsx';
import AddTicket from 'views/linguist/Ticket/AddTicket.jsx';
import EditTicket from 'views/linguist/Ticket/EditTicket.jsx';

import LinguistMailinbox from 'views/linguist/Mail/Inbox.jsx';
import LinguistMailcompose from 'views/linguist/Mail/Compose.jsx';
import LinguistMailview from 'views/linguist/Mail/View.jsx';

import Bid from 'views/linguist/Bid/Bid.jsx';
import AddBid from 'views/linguist/Bid/AddBid.jsx';
import EditBid from 'views/linguist/Bid/EditBid.jsx';

import LinguistReportsProjects from 'views/linguist/Reports/ReportsProjects.jsx'; 
import LinguistReportsBuyers from 'views/linguist/Reports/ReportsBuyers.jsx'; 
import LinguistReportsTargets from 'views/linguist/Reports/ReportsTargets.jsx'; 


var BASEDIR = process.env.REACT_APP_BASEDIR;

var dashRoutes = [ 

    //{ path: "#", name: "Main", type: "navgroup"},
    { path: BASEDIR+"/admin/dashboard", name: "Dashboard", icon: "speedometer", badge: "", component: Admin },

    { 
        path: "#", name: "Customers", icon: "people", type: "dropdown", parentid: "buyers",
            child: [
                { path: BASEDIR+"/linguist/buyers", name: "Buyers"},
                { path: BASEDIR+"/linguist/add-buyer", name: "Add Buyer"},
                { path: BASEDIR+"/linguist/edit-buyer", name: "Edit Buyer"},
            ]
    },
        { path: BASEDIR+"/linguist/buyers", component: Buyer, type: "child"},
        { path: BASEDIR+"/linguist/add-buyer", component: AddBuyer, type: "child"},
        { path: BASEDIR+"/linguist/edit-buyer", component: EditBuyer, type: "child"},

    { 
        path: "#", name: "Linguists", icon: "user", type: "dropdown", parentid: "sellers",
            child: [
                { path: BASEDIR+"/linguist/sellers", name: "Sellers"},
                { path: BASEDIR+"/linguist/add-seller", name: "Add Seller"},
                { path: BASEDIR+"/linguist/edit-seller", name: "Edit Seller"},
            ]
    },
        { path: BASEDIR+"/linguist/sellers", component: Seller, type: "child"},
        { path: BASEDIR+"/linguist/add-seller", component: AddSeller, type: "child"},
        { path: BASEDIR+"/linguist/edit-seller", component: EditSeller, type: "child"},
        
    { 
        path: "#", name: "Projects", icon: "note", type: "dropdown", parentid: "projectbids",
            child: [
                { path: BASEDIR+"/linguist/bids", name: "Bids"},
                { path: BASEDIR+"/linguist/add-bid", name: "Add Bid"},
                { path: BASEDIR+"/linguist/edit-bid", name: "Edit Bid"},
            ]
    },
        { path: BASEDIR+"/linguist/bids", component: Bid, type: "child"},
        { path: BASEDIR+"/linguist/add-bid", component: AddBid, type: "child"},
        { path: BASEDIR+"/linguist/edit-bid", component: EditBid, type: "child"},

    { 
        path: "#", name: "Billing", icon: "wallet", type: "dropdown", parentid: "billing",
            child: [
                { path: BASEDIR+"/linguist/invoices", name: "Invoices"},
                { path: BASEDIR+"/linguist/add-invoice", name: "Add Invoice"},
                { path: BASEDIR+"/linguist/edit-invoice", name: "Edit Invoice"},
            ]
    },
        { path: BASEDIR+"/linguist/invoices", component: Invoice, type: "child"},
        { path: BASEDIR+"/linguist/add-invoice", component: AddInvoice, type: "child"},
        { path: BASEDIR+"/linguist/edit-invoice", component: EditInvoice, type: "child"},

    { 
        path: "#", name: "Reports", icon: "graph", type: "dropdown", parentid: "reports",
        child: [
                    { path: BASEDIR+"/linguist/reports-projects", name: "Projects"},
                    { path: BASEDIR+"/linguist/reports-buyers", name: "Buyers"},
                    { path: BASEDIR+"/linguist/reports-targets", name: "Targets"},
        ]
    },
    { path: BASEDIR+"/linguist/reports-projects", component: LinguistReportsProjects, type: "child"},
    { path: BASEDIR+"/linguist/reports-buyers", component: LinguistReportsBuyers, type: "child"},
    { path: BASEDIR+"/linguist/reports-targets", component: LinguistReportsTargets, type: "child"},


    { 
        path: "#", name: "Disputes", icon: "question", type: "dropdown", parentid: "tickets",
            child: [
                { path: BASEDIR+"/linguist/tickets", name: "Tickets"},
                { path: BASEDIR+"/linguist/add-ticket", name: "Add Ticket"},
                { path: BASEDIR+"/linguist/edit-ticket", name: "Edit Ticket"},
            ]
    },
        { path: BASEDIR+"/linguist/tickets", component: Ticket, type: "child"},
        { path: BASEDIR+"/linguist/add-ticket", component: AddTicket, type: "child"},
        { path: BASEDIR+"/linguist/edit-ticket", component: EditTicket, type: "child"},

    { 
        path: "#", name: "Users", icon: "user-female", type: "dropdown", parentid: "users",
            child: [
                { path: BASEDIR+"/linguist/users", name: "Users"},
                { path: BASEDIR+"/linguist/add-user", name: "Add User"},
                { path: BASEDIR+"/linguist/edit-user", name: "Edit User"},
            ]
    },
        { path: BASEDIR+"/linguist/users", component: User, type: "child"},
        { path: BASEDIR+"/linguist/add-user", component: AddUser, type: "child"},
        { path: BASEDIR+"/linguist/edit-user", component: EditUser, type: "child"},

    { 
        path: "#", name: "Messages", icon: "speech", type: "dropdown", parentid: "mailbox",
        child: [
            { path: BASEDIR+"/linguist/mail-inbox", name: "Inbox"},
            { path: BASEDIR+"/linguist/mail-compose", name: "Compose"},
            { path: BASEDIR+"/linguist/mail-view", name: "View"},
        ]
    },
        { path: BASEDIR+"/linguist/mail-inbox", component: LinguistMailinbox, type: "child"},
        { path: BASEDIR+"/linguist/mail-compose", component: LinguistMailcompose, type: "child"},
        { path: BASEDIR+"/linguist/mail-view", component: LinguistMailview, type: "child"},

    { 
        path: "#", name: "Internal Messages", icon: "envelope", type: "dropdown", parentid: "mailbox",
        child: [
            { path: BASEDIR+"/linguist/mail-inbox", name: "Inbox"},
            { path: BASEDIR+"/linguist/mail-compose", name: "Compose"},
            { path: BASEDIR+"/linguist/mail-view", name: "View"},
        ]
    },
        { path: BASEDIR+"/linguist/mail-inbox", component: LinguistMailinbox, type: "child"},
        { path: BASEDIR+"/linguist/mail-compose", component: LinguistMailcompose, type: "child"},
        { path: BASEDIR+"/linguist/mail-view", component: LinguistMailview, type: "child"},
];

export default dashRoutes;
