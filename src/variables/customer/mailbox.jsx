// ##############################
// // // mailbox
// #############################
var IMGDIR = process.env.REACT_APP_IMGDIR;

const mailbox = [
    {avatar: IMGDIR+"/images/profile/avatar-1.jpg", name: "Michael Harris", time: "15 mins ago", tag: "Linguist", star: "1", msg: 'I will start work on your project ASAP'},
    {avatar: IMGDIR+"/images/profile/avatar-2.jpg", name: "Luke Shaw", time: "35 mins ago", tag: "Admin", star: "0", msg: 'Your Dispute has been received. We will contact you shortly.'},
    {avatar: IMGDIR+"/images/profile/avatar-5.jpg", name: "Eric Ellison", time: "54 mins ago", tag: "Linguist", star: "1", msg: 'Thanks for the review!'},
    {avatar: IMGDIR+"/images/profile/avatar-6.jpg", name: "Evan Shaw", time: "58 mins ago", tag: "Admin", star: "0", msg: 'Concerning Dispute No. 443, please find enclosed...'},
    {avatar: IMGDIR+"/images/profile/avatar-9.jpg", name: "Austin Kerr", time: "2 hours ago", tag: "Linguist", star: "1", msg: 'Take cafe and meet soon this weekend'},
    {avatar: IMGDIR+"/images/profile/avatar-10.jpg", name: "Boris King", time: "3 hours ago", tag: "Admin", star: "0", msg: 'Thank you for your understanding'},
];

export {
    mailbox, // mail inbox
};
