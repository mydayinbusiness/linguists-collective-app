// ##############################
// // // mailbox
// #############################
var IMGDIR = process.env.REACT_APP_IMGDIR;

const mailbox = [
    {avatar: IMGDIR+"/images/profile/avatar-2.jpg", name: "Zara Khan", time: "35 mins ago", tag: "Admin", star: "0", msg: 'Please close Job No.33. Thanks'},
    {avatar: IMGDIR+"/images/profile/avatar-3.jpg", name: "Natalie Graham", time: "45 mins ago", tag: "Customer", star: "0", msg: 'Thanks for the quick turnaround! :)'},
    {avatar: IMGDIR+"/images/profile/avatar-6.jpg", name: "Abdul Ahmed", time: "58 mins ago", tag: "Admin", star: "0", msg: 'Customer has raised dispute over Job No. 3834 etc'},
    {avatar: IMGDIR+"/images/profile/avatar-7.jpg", name: "Sarah Smith", time: "1 hour ago", tag: "Customer", star: "0", msg: 'Please do the needful.'},
    {avatar: IMGDIR+"/images/profile/avatar-11.jpg", name: "Jane Lewis", time: "4 hours ago", tag: "Customer", star: "0", msg: 'Thanks.'},
];

export {
    mailbox, // mail inbox
};
