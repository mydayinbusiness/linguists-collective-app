import Customer from 'views/customer/Dashboard/Customer.jsx';

import Invoice from 'views/linguist/Invoice/Invoice.jsx';
import AddInvoice from 'views/linguist/Invoice/AddInvoice.jsx';
import EditInvoice from 'views/linguist/Invoice/EditInvoice.jsx';

import Ticket from 'views/linguist/Ticket/Ticket.jsx';

import LinguistMailinbox from 'views/linguist/Mail/Inbox.jsx';
import LinguistMailcompose from 'views/linguist/Mail/Compose.jsx';
import LinguistMailview from 'views/linguist/Mail/View.jsx';

import Bid from 'views/linguist/Bid/Bid.jsx';
import AddBid from 'views/linguist/Bid/AddBid.jsx';

import LinguistReportsProjects from 'views/linguist/Reports/ReportsProjects.jsx'; 
import LinguistReportsBuyers from 'views/linguist/Reports/ReportsBuyers.jsx'; 
import LinguistReportsTargets from 'views/linguist/Reports/ReportsTargets.jsx'; 


var BASEDIR = process.env.REACT_APP_BASEDIR;

var dashRoutes = [ 

    //{ path: "#", name: "Main", type: "navgroup"},
    { path: BASEDIR+"/customer/dashboard", name: "Dashboard", icon: "speedometer", badge: "", component: Customer },
    
    { 
        path: "#", name: "Projects", icon: "note", type: "dropdown", parentid: "projectbids",
            child: [
                { path: BASEDIR+"/linguist/bids", name: "Projects"},
                { path: BASEDIR+"/linguist/add-bid", name: "Create Project"},
            ]
    },
        { path: BASEDIR+"/linguist/bids", component: Bid, type: "child"},
        { path: BASEDIR+"/linguist/add-bid", component: AddBid, type: "child"},

    { 
        path: "#", name: "Billing", icon: "wallet", type: "dropdown", parentid: "billing",
            child: [
                { path: BASEDIR+"/linguist/invoices", name: "Invoices"},
                { path: BASEDIR+"/linguist/add-invoice", name: "Add Invoice"},
                { path: BASEDIR+"/linguist/edit-invoice", name: "Edit Invoice"},
            ]
    },
        { path: BASEDIR+"/linguist/invoices", component: Invoice, type: "child"},
        { path: BASEDIR+"/linguist/add-invoice", component: AddInvoice, type: "child"},
        { path: BASEDIR+"/linguist/edit-invoice", component: EditInvoice, type: "child"},

    { 
        path: "#", name: "Reports", icon: "graph", type: "dropdown", parentid: "reports",
        child: [
                    { path: BASEDIR+"/linguist/reports-projects", name: "Projects"},
                    { path: BASEDIR+"/linguist/reports-buyers", name: "Buyers"},
                    { path: BASEDIR+"/linguist/reports-targets", name: "Targets"},
        ]
    },
    { path: BASEDIR+"/linguist/reports-projects", component: LinguistReportsProjects, type: "child"},
    { path: BASEDIR+"/linguist/reports-buyers", component: LinguistReportsBuyers, type: "child"},
    { path: BASEDIR+"/linguist/reports-targets", component: LinguistReportsTargets, type: "child"},


    { 
        path: "#", name: "Disputes", icon: "question", type: "dropdown", parentid: "tickets",
            child: [
                { path: BASEDIR+"/linguist/tickets", name: "Disputes"},
            ]
    },
        { path: BASEDIR+"/linguist/tickets", component: Ticket, type: "child"},

    { 
        path: "#", name: "Messages", icon: "speech", type: "dropdown", parentid: "mailbox",
        child: [
            { path: BASEDIR+"/linguist/mail-inbox", name: "Inbox"},
            { path: BASEDIR+"/linguist/mail-compose", name: "Compose"},
            { path: BASEDIR+"/linguist/mail-view", name: "View"},
        ]
    },
    { path: BASEDIR+"/linguist/mail-inbox", component: LinguistMailinbox, type: "child"},
    { path: BASEDIR+"/linguist/mail-compose", component: LinguistMailcompose, type: "child"},
    { path: BASEDIR+"/linguist/mail-view", component: LinguistMailview, type: "child"},
];
export default dashRoutes;
