import React from 'react';
// used for making the prop types of this component
import PropTypes from 'prop-types';

class ProjectCard extends React.Component {
    render(){
        return (
            <li class='project-card'>
                <h3>{this.props.project} ({this.props.number})</h3> <br />
                <p>{this.props.buyer}</p>
            </li>
        );
    }
}

ProjectCard.propTypes = {
    default: PropTypes.bool,
    number: PropTypes.string,
    buyer: PropTypes.string,
    project: PropTypes.string,
}

export default ProjectCard;
